#include <stdexcept>
#include <iostream>
#include <vector>
#include <algorithm>

class UniqueNumbers
{
public:
    static std::vector<int> findUniqueNumbers(const std::vector<int>& numbers)
    {
        //throw std::logic_error("Waiting to be implemented");
        // std::vector<int> retVector;
        // std::vector<int> inputVector(numbers);
        
        // for(auto it=inputVector.begin(); it < inputVector.end(); )
        // {
        //     if(std::count(inputVector.begin(), inputVector.end(), *it) == 1)
        //     {
        //          retVector.push_back(*it); 
        //     }
        //          it++;
        //    //  else
        //  		// it = inputVector.erase(it);   	      
        // }
        std::vector<int> inputVector(numbers);
        //std::vector<int> retVector;
        std::sort(inputVector.begin(), inputVector.end());//N*logN
        auto it = std::unique(inputVector.begin(), inputVector.end()); //N
        inputVector.resize(std::distance(inputVector.begin(), it));
        
        return inputVector;
    }
};

#ifndef RunTests
int main()
{
    std::vector<int> numbers = { 1, 2, 1, 3 };
    std::vector<int> result = UniqueNumbers::findUniqueNumbers(numbers);
    for (int i = 0; i < result.size(); i++)
        std::cout << result[i] << '\n';
}
#endif